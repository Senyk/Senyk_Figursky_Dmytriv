#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <time.h>
#include <conio.h>
#define N 10000
using namespace std;
int a[N];
void quick(int p, int r)
{
	int k = 0;
	int i = p;
	int j = r;
	int x = a[(p + r) / 2];
	while (i <= j)
	{
		while (a[i] < x){ i++; k++; }
		while (a[j] > x){ j--; k++; }

		if (i <= j)
		{
			swap(a[i], a[j]);

			i++;
			j--;
		}
	}
	if (i<r){ quick(i, r); }
	if (p<j){ quick(p, j); }
	cout << k << endl;
}

int main()
{
	
	int i, n;
	ifstream fin;
	fin.open("file.txt");
	if (!fin.is_open()){
		cout << "File can not be opened!\n";
		return 0;
	}
	cout << "N=";
	try
	{
	cin >> n;
		if (n > 10000)
			{
				throw "Variable n is the number of more than 10000, if you want to continue, please click Enter,but program will not work correct ";
			}
		}
		catch (const char* message)
		{
			cout << " error! "<< message << endl;
			_getch();
		}
	for (i = 0; i<n; i++)
	{
		cout << "A[" << i + 1 << "]=";
		fin >> a[i];
	}
	quick(0, n - 1);
	for (i = 0; i < n; i++)
	{
		cout << "A[" << i + 1 << "]=" << a[i] << endl;
	}
	_getch();
}
