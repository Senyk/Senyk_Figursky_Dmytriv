#include<iostream>
#include<string.h>
#include<stdio.h>
#define MAX 3
using namespace std;
int getMax(int arr[], int n)
{
    int mx = arr[0];
    for (int i = 1; i < n; i++)
        if (arr[i] > mx)
            mx = arr[i];
    return mx;
}
 
void countSort(int arr[], int n, int exp)
{
    int output[n]; 
    int i, count[10] = {0};
 
    for (i = 0; i < n; i++)
        count[ (arr[i]/exp)%10 ]++;
 
    
    for (i = 1; i < 10; i++)
        count[i] += count[i - 1];
 
    
    for (i = n - 1; i >= 0; i--)
    {
        output[count[ (arr[i]/exp)%10 ] - 1] = arr[i];
        count[ (arr[i]/exp)%10 ]--;
    }
 

    for (i = 0; i < n; i++)
        arr[i] = output[i];
}
 

void radixsort(int arr[], int n)
{
    
    int m = getMax(arr, n);
 
    
    for (int exp = 1; m/exp > 0; exp *= 10)
        countSort(arr, n, exp);
}
 

void print(int arr[], int n)
{
    for (int i = 0; i < n; i++)
        cout << arr[i] << " ";
}
int from_char_to_int(char str)
{
    char alphavet[]="abcdefghijklmnopqrstuvwxyz";
    for (int i = 0; i < 26; i++)
    {
        if(str==alphavet[i])
        return i;
    }
}
char from_int_to_char(int num)
{
    char alphavet[]="abcdefghijklmnopqrstuvwxyz";
    return alphavet[num];
}

void convent_char_to_int(char str[],int a[])
     {
         for (int i = 0; i < MAX; i++)
            {
                a[i]=from_char_to_int(str[i]);
            }                     
     }
void convent_int_to_char(char *str,int a[])
     {
         for (int i = 0; i < MAX; i++)
            {
                str[i]=from_int_to_char(a[i]);
            }                     
     }
int print_often(int array[][MAX]){
    int arr [30];int arr_counter=0;
    for(int i = 0; i < 10; i++)
            for(int j = 0; j < MAX; j++){
                    arr[arr_counter]=array[i][j];
                    arr_counter++;
                    }
    int N = 30,CurrentCounter = 0, BiggestCounter = 0, FrequentEl = 0;
    for(int i = 0; i < N; i++)
    {
        for(int j = i; j < N; j++)
        {
            if(arr[i] == arr[j])
            {
                 CurrentCounter++;
            }
        }
        if(CurrentCounter > BiggestCounter)
        {
             BiggestCounter = CurrentCounter;
             FrequentEl = arr[i];
        }
        CurrentCounter = 0;
    }
    cout << from_int_to_char(FrequentEl);
    }
 

int main()
{
    char string [10][MAX];
    string[0][0]='h';
    string[0][1]='z';
    string[0][2]='t';
    
    string[1][0]='s';
    string[1][1]='n';
    string[1][2]='g';
    
    string[2][0]='e';
    string[2][1]='n';
    string[2][2]='a';
    
    string[3][0]='s';
    string[3][1]='d';
    string[3][2]='t';
    
    string[4][0]='q';
    string[4][1]='d';
    string[4][2]='s';
    
    string[5][0]='y';
    string[5][1]='i';
    string[5][2]='f';
    
    string[6][0]='s';
    string[6][1]='l';
    string[6][2]='t';
    
    string[7][0]='l';
    string[7][1]='p';
    string[7][2]='z';
    
    string[8][0]='c';
    string[8][1]='q';
    string[8][2]='c';
    
    string[9][0]='h';
    string[9][1]='p';
    string[9][2]='o';
    
    int a[10][MAX];
    int to_sort[10];
    for (int i = 0; i < 10; i++)
            {
                convent_char_to_int(string[i],a[i]);
            }
            for (int i = 0; i < 10; i++)
            {
                to_sort[i]=0;
                for (int j = 0; j < MAX; j++)
                {
                 if(j==2)to_sort[i]+=a[i][j];
                 if(j==1)to_sort[i]+=a[i][j]*100;
                 if(j==0)to_sort[i]+=a[i][j]*10000;
                 }
            }
     int n = sizeof(to_sort)/sizeof(to_sort[0]);       
     radixsort(to_sort, n);
     
     int first_first=to_sort[0]%100;
     int first_second=to_sort[0]/10000;
     int first_middle=(to_sort[0]-first_first-first_second*10000)/100;
     
     cout<<from_int_to_char(first_second)<<from_int_to_char(first_middle)<<from_int_to_char(first_first);
     
     print_often(a);
     
     int last_first=to_sort[9]%100;
     int last_second=to_sort[9]/10000;
     int last_middle=(to_sort[9]-last_first-last_second*10000)/100;
     
     cout<<from_int_to_char(last_second)<<from_int_to_char(last_middle)<<from_int_to_char(last_first)<<endl;
     
     cin.get();
     return 0;
}